package com.example.demo.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Item;
import com.example.demo.repository.ItemRepository;
import com.example.demo.service.ItemService;
@Service
public class ItemServiceImplement implements ItemService {
	private  ItemRepository itemRepository;
	
	@Autowired
	public ItemServiceImplement(ItemRepository itemRepository) {
		super();
		this.itemRepository = itemRepository;
	}

	@Override
	public List<Item> showAll() {
		
		return itemRepository.showAll();
	}

	@Override
	public void addNew(Item item) {
		
		itemRepository.addNew(item);
		
	}

	@Override
	public Item findOne(int id) {
		
		return itemRepository.findOne(id);
	}

	@Override
	public void delete(int id) {
		itemRepository.delete(id);
		
	}
	

}
