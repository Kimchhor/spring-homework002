package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Item;

public interface ItemService {
	public List<Item> showAll();
	public void addNew(Item item);
	public Item findOne(int id);
	public void delete(int id);
	
}
