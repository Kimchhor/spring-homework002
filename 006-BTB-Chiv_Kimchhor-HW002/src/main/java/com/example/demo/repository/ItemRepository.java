package com.example.demo.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.demo.model.Item;
@Repository
public class ItemRepository {
	List<Item> itemList=new ArrayList<>();
	{
		itemList.add(new Item(1,"item1","this is item1 "));
		itemList.add(new Item(2,"item2","this is item2"));
		itemList.add(new Item(3,"item3","this is item3"));
	}
	
	public List<Item> showAll(){
		return itemList;
	}
	
	public void addNew(Item item) {
		
		itemList.add(item);
	}
	
	public Item findOne(int id) {
		
		Item item=itemList.get(id-1);
		return item;
	}
	
	public void delete(int id) {
		
		for(Item item:itemList) {
			if(item.getId()==id) {
				itemList.remove(item);
				return;
			}
		}
	}
	
	
	
}
