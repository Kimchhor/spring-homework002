package com.example.demo.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.Item;
import com.example.demo.service.ItemService;


@Controller
@RequestMapping("/items")
public class ItemController {
	private ItemService itemService;
	
	@Autowired
	public ItemController(ItemService itemService) {
		super();
		this.itemService = itemService;
	}

	@GetMapping
	public String showAll(ModelMap map) {
		map.addAttribute("listItem", itemService.showAll());
		
		return "index";
	}
	
	@GetMapping("/createNew")
	public String getForm(ModelMap modelMap) {
		modelMap.addAttribute("item",new Item());
		return("createNew");
	}
	
	@PostMapping("/add")
	public String save(@ModelAttribute Item item ) {
		item.setId(itemService.showAll().size()+1);
		itemService.addNew(item);
		return("redirect:/items");
	}
	@GetMapping("/view")
	public String showView() {
		
		return("view");
	}
	@GetMapping("/view/{id}")
	public String view(@PathVariable int id) {
		itemService.findOne(id);
		return("redirect:/view");
		
	}
	
	
	@GetMapping("/delete/{id}")
	public String delete( @PathVariable int id) {
		
		itemService.delete(id);
		return"redirect:/items";
	}
}
