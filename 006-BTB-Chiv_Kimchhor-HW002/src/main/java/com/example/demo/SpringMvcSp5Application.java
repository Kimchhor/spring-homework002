package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcSp5Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcSp5Application.class, args);
	}

}
